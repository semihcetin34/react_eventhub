import React, {useState} from "react";
import eventHub from "../eventHub";
const ComponentOne=()=>{
    const [one,set_one]=useState(1)
    const [value,set_value]=useState(1)
    eventHub.on('one',data=>{
       set_one(data) ;
        set_value(value+1);
    });
    return <div>
        <button onClick={()=>eventHub.trigger("two",'this value senden from 1')}>Component One</button>
        <p>Component One trigger value {one} _ {value}</p>
    </div>
}
export default ComponentOne
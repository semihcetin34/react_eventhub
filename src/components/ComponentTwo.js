import React, {useState} from "react";
import eventHub from "../eventHub";
const ComponentTwo=()=>{
    const [two,set_two]=useState(1)
    const [value,set_value]=useState(1)
    eventHub.on('two',data=>{
        set_two(data) ;
        set_value(value+1);
    });
    return <div>
        <button onClick={()=>eventHub.trigger("one",'this value senden from 2')}>component two</button>
        <p>Component Two trigger value {two} _ {value}</p>
    </div>
}
export default ComponentTwo